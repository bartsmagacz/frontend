To run project type ```npm run start``` or ```yarn start```

This project contains the Todo List. The status of every item can be changed, and every item can be removed.
This project is added a simple form to add a new todo item.

Requirements:
- Node LTS (12.x)
- Running backend service

Used technologies:
- `React`
- `Material-UI`
- `React Hooks`
- `Jest`
- `React Testing Library`
- `StandardJS`

Potential improvements:
- Add sorting of items (based on status)
- Notification (if the request went well or not)
- Add `Redux`
- Better styling
- Implement `docker`
- Add more CI/CD to a project (build, deploy)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
