export const fetchTodoItems = async () => {
  const response = await fetch(`${process.env.REACT_APP_BACKEND_URL}/v1/todo`)
  return await response.json()
}

// body: {
//  _id: id,
//  state: newState
// }
export const updateTodoItem = async (body) => {
  const response = await fetch(`${process.env.REACT_APP_BACKEND_URL}/v1/todo`, {
    method: 'PUT',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
  return await response.json()
}

export const deleteTodoItem = async (body) => {
  const response = await fetch(`${process.env.REACT_APP_BACKEND_URL}/v1/todo`, {
    method: 'DELETE',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
  return await response.json()
}

export const addTodoItem = async (body) => {
  const response = await fetch(`${process.env.REACT_APP_BACKEND_URL}/v1/todo`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
  return await response.json()
}
