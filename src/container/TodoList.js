import React, { useState, useEffect } from 'react'
import { List } from '@material-ui/core'
import { TodoItem } from '../components/TodoItem'
import { addTodoItem, deleteTodoItem, fetchTodoItems, updateTodoItem } from '../api/todoApi'
import { AddItem } from '../components/AddItem'

const TodoListRender = () => {
  const [todoItems, setTodoItems] = useState([])

  useEffect(() => {
    const fetchTodoItemsReq = async () => {
      setTodoItems(await fetchTodoItems())
    }
    fetchTodoItemsReq()
  }, [])

  const handleUpdate = async (id, state) => {
    const updatedItem = await updateTodoItem({ _id: id, state })
    setTodoItems(prevState => {
      return prevState.map((item) => {
        if (item._id === updatedItem._id) {
          return updatedItem
        }
        return item
      })
    })
  }

  const handleDelete = async (id) => {
    const deletedItem = await deleteTodoItem({ _id: id })
    setTodoItems(prevState => {
      return prevState.filter(item => {
        if (item._id !== deletedItem._id) {
          return true
        }
      })
    })
  }

  const handleAdd = async (itemToAdd) => {
    const addedItem = await addTodoItem(itemToAdd)
    setTodoItems(prevState => ([...prevState, addedItem]))
  }

  return (
    <List>
      {todoItems && todoItems.map((item, index) =>
        <TodoItem
          key={`todoItem-${index}`}
          item={item}
          handleUpdate={handleUpdate}
          handleDelete={handleDelete}
        />)}
      <AddItem handleAdd={handleAdd} />
    </List>)
}

export const TodoList = TodoListRender
