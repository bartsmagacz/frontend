import React from 'react'
import { render } from '@testing-library/react'
import App from './App'

test('render App Component', () => {
  const { asFragment } = render(<App />)
  expect(asFragment()).toMatchSnapshot()
})
