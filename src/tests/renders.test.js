/* eslint-disable react/jsx-closing-bracket-location */

import React from 'react'
import { render } from '@testing-library/react'
import { StateIcon } from '../components/StateIcon'
import { STATES } from '../consts'
import { TodoItem } from '../components/TodoItem'
import { UpdateStateButton } from '../components/UpdateStateButton'
import { DeleteItem } from '../components/DeleteItem'
import { AddItem } from '../components/AddItem'

const statesArray = [
  STATES.OPEN, STATES.PENDING, STATES.CLOSED
]

test.each(statesArray)('Render - State Icon Component - %s', (state) => {
  const { asFragment } = render(<StateIcon state={state} />)
  expect(asFragment()).toMatchSnapshot()
})

test('Render - TodoItem', () => {
  const { asFragment } = render(<TodoItem
    handleUpdate={() => {}} item={{
      _id: 'debugId123',
      state: 'open',
      title: 'debugTitle',
      description: 'debugDescription'
    }}
  />)
  expect(asFragment()).toMatchSnapshot()
})

test.each(statesArray)('Render - Update State Button - %s', (state) => {
  const { asFragment } = render(<UpdateStateButton state={state} />)
  expect(asFragment()).toMatchSnapshot()
})

test('Render - Delete Button', () => {
  const { asFragment } = render(<DeleteItem handleDelete={() => {}} _id='randomId' />)
  expect(asFragment()).toMatchSnapshot()
})

test('Render - Add Item', () => {
  const { asFragment } = render(<AddItem handleAdd={() => {}} />)
  expect(asFragment()).toMatchSnapshot()
})
