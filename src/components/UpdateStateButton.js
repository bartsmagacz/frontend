import React from 'react'
import { STATES } from '../consts'
import Button from '@material-ui/core/Button'

const UpdateStateButtonRender = ({ id, state, handleUpdate }) => {
  return (
    <>
      {state === STATES.OPEN && <Button variant='contained' color='primary' onClick={() => handleUpdate(id, STATES.PENDING)}>PROGRESS</Button>}
      {state === STATES.PENDING && <Button variant='contained' color='secondary' onClick={() => handleUpdate(id, STATES.CLOSED)}>DONE</Button>}
      {state === STATES.CLOSED && <Button variant='contained' disabled>CLOSED</Button>}
    </>
  )
}

export const UpdateStateButton = UpdateStateButtonRender
