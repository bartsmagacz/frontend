import React from 'react'
import { ListItem, ListItemIcon, ListItemText, ListItemSecondaryAction } from '@material-ui/core'
import { StateIcon } from './StateIcon'
import { UpdateStateButton } from './UpdateStateButton'
import { DeleteItem } from './DeleteItem'

const TodoItemRender = ({ item, handleUpdate, handleDelete, handleAdd }) => {
  return (
    <ListItem>
      <ListItemIcon>
        <StateIcon state={item.state} />
      </ListItemIcon>
      <ListItemText
        primary={item.title}
        secondary={item.description}
      />
      <ListItemSecondaryAction>
        <UpdateStateButton id={item._id} state={item.state} handleUpdate={handleUpdate} />
        <DeleteItem id={item._id} handleDelete={handleDelete} />
      </ListItemSecondaryAction>
    </ListItem>
  )
}

export const TodoItem = TodoItemRender
