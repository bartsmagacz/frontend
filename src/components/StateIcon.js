import React from 'react'
import { STATES } from '../consts.js'
import DoneIcon from '@material-ui/icons/Done'
import UpdateIcon from '@material-ui/icons/Update'
import NewReleasesIcon from '@material-ui/icons/NewReleases'

const StateIconRender = ({ state }) => {
  return (
    <>
      {state === STATES.OPEN && <NewReleasesIcon />}
      {state === STATES.PENDING && <UpdateIcon />}
      {state === STATES.CLOSED && <DoneIcon />}
    </>
  )
}

export const StateIcon = StateIconRender
