import React from 'react'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'

const DeleteItemRender = ({ id, handleDelete }) => {
  return (
    <IconButton onClick={() => handleDelete(id)}>
      <DeleteIcon />
    </IconButton>
  )
}

export const DeleteItem = DeleteItemRender
