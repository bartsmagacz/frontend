import React, { useState } from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

const AddItemRender = ({ handleAdd }) => {
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState()

  const handleTitleChange = e => {
    setTitle(e.target.value)
  }

  const handleDescriptionChange = e => {
    setDescription(e.target.value)
  }

  return (
    <form noValidate autoComplete='off'>
      <Grid container spacing={1}>
        <Grid item xs>
          <TextField id='outlined-basic' label='Title' variant='outlined' value={title} onChange={handleTitleChange} />
        </Grid>
        <Grid item xs>
          <TextField
            label='Description'
            multiline
            rows={4}
            variant='outlined'
            value={description}
            onChange={handleDescriptionChange}
          />
        </Grid>
        <Grid item xs>
          <Button variant='contained' color='primary' onClick={() => handleAdd({ title, description })}>Add Item</Button>
        </Grid>
      </Grid>
    </form>
  )
}

export const AddItem = AddItemRender
