import React from 'react'
import { Container } from '@material-ui/core'
import { TodoList } from './container/TodoList'
import './App.css'

function App () {
  return (
    <div className='App'>
      <Container component='main' maxWidth='md'>
        <TodoList />
      </Container>
    </div>
  )
}

export default App
